// Là một object có thể chứa toàn bộ các option của một component( data, method, hooks)
// khi sử dụng nó tất cả components sẽ kế thừa tất cả những gì mixin đó có
// khi bị ghi đè thì nội dung được nhận sẽ là của components sử dụng nó

export default {
  created() {
    this.myEventHandler()
    window.addEventListener('resize', this.myEventHandler)
  },
  data() {
    return {
      company: window.companyPath,
      token: window.CSRF_TOKEN,
      isMobile: false,
      isTablet: false,
      isDesktop: false,
    }
  },
  methods: {
    myEventHandler() {
      if (window.innerWidth < 768) {
        this.isMobile = true
        this.isTablet = false
        this.isDesktop = false
      } else if (window.innerWidth == 768) {
        this.isTablet = true
        this.isMobile = false
        this.isDesktop = false
      } else if (window.innerWidth > 768) {
        this.isDesktop = true
        this.isMobile = false
        this.isTablet = false
      }
    },
  },
  destroyed() {
    window.removeEventListener('resize', this.myEventHandler)
  },
}
