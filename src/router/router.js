import Vue from 'vue'
//Dòng này để import vue-router
import Router from 'vue-router'
// import HelloWordParent from '@/components/HelloWordParent'
// import About from '@/components/About'


//  import như thế này sẽ lazy loading
const HelloWordParent = () => import('@/components/HelloWordParent')
const About = () => import('@/components/About')

Vue.use(Router)

export default new Router({
  routes: [ // bao gồm danh sách route
    {
      path: '/hello', ///path của route
      name: 'Hello', // tên route
      component: HelloWordParent, // component route sử dụng,
      beforeEnter: async (to, from, next) => {  // được gọi trứoc khi hiển thị component
        // to: thông tin url đang muốn đến
        // from thông tin url khi vào url này
        console.log('from: ', from);
        console.log('to: ', to);
        // return true hoặc next() để cho vào url này
        next()
        return true
      },
      scrollBehavior () {  // khi vào trang này, thanh cuộn sẽ được đẩy lên đầu
        return { x: 0, y: 0 }
      }
    },
    {
      path: '/about',
      name: 'About',
      component: About
    }
  ]
})