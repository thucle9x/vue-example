export const name = "countStore";

export default {
  namespaced: true,
  state: {
    count: 100,
  },
  getters: {
    getCount(state) {
      return state.count;
    },
    getOtherCount: (state) => state.count
  },
  //  dựa vào các action để thay đổi state
  mutations: {
    CHANGE_COUNT(state, payload) {   // nếu đồng bộ thì có thể dispatch luôn mutation  store.dispatch('CHANGE_COUNT')
      state.count = payload;
    },
  },
  // Nơi định nghĩa các action, khi action được gọi có thể call api
  actions: {
    changeCount({ commit }, payload) {
      commit("CHANGE_COUNT", payload);
    },
  },
};


// doc https://vuex.vuejs.org/guide/modules.html#accessing-global-assets-in-namespaced-modules
