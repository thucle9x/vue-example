import Vue from 'vue'
import Vuex from 'vuex'
import countStore from "./count"
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    countStore,
  }
})
export default store
// Tham khảo
// https://vntalking.com/su-dung-thu-vien-vuex-de-quan-ly-state-trong-vuejs.html
// https://viblo.asia/p/su-dung-vuex-de-quan-ly-state-trong-vuejs-djeZ1D4mKWz